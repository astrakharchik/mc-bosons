/*spline.c*/
#include <stdlib.h>
#include <stdio.h>
#include "spline.h"
#include "main.h"
#include "utils.h"
#include "trial.h"
#include "memory.h"
#include "compatab.h"
#include MATHINCLUDE 

#define SPLINE_EPSILON 1e-10

/****************************** SaveWaveFunctionSpline *****************************/
void SaveWaveFunctionSpline(struct Grid *G, char *file_out) {
  int i;
  int Nspline_export = 1000;
  FILE *out;
  DOUBLE x, yInter, ypInter, yppInter;
  static int first_time = ON;

  out = fopen(file_out, "w");

  if(first_time) Message("Saving wavefunction (spline).\n");

  fprintf(out, "x f fp fpp\n");

  if(Nspline_export>10*G->size) Nspline_export = G->size;

  for(i=0; i<Nspline_export; i++) {
    x = G->min + 0.1*G->step * (DOUBLE) i;
    InterpolateSpline(G, x, &yInter, &ypInter, &yppInter);
    //yInter = Exp(InterpolateSplineU(G, x));
    //ypInter = InterpolateSplineFp(G, x);
    //yppInter = InterpolateSplineE(G, x);

    //if(yInter<0) Message("  Bad value of Spline function (negative), distance %" LF " = %" LF " x dx, value %" LF "\n", x, 0.1*(DOUBLE) i, yInter);
    fprintf(out, "%.15" LF " %.15" LF " %.15" LF " %.15" LF "\n", x, yInter, ypInter, yppInter);
  }

  fclose(out);

  first_time = OFF;
}

/************************************ spline construction *******************************/
void SplineConstruct(struct Grid *G, DOUBLE yp1, DOUBLE ypn) {
  int i, k;
  DOUBLE p, qn, sig, un;
  int n;

  n = G->size;

  //G->fpp[0] = G->fp[0] = 0.; // natural spline
  if(yp1>1e30) // zero second derivative
    G->fpp[0] = G->fp[0]=0.;
  else { // first derivative equal to yp1
    G->fpp[0] = -0.5;
    G->fp[0] = (3./(G->x[1]-G->x[0]))*((G->f[1]-G->f[0])/(G->x[1]-G->x[0])-yp1);
  }

  for(i=1; i<n-1; i++) {
    sig=(G->x[i]-G->x[i-1])/(G->x[i+1]-G->x[i-1]);
    p=sig*G->fpp[i-1]+2.0;
    G->fpp[i]=(sig-1.0)/p;
    G->fp[i]=(G->f[i+1]-G->f[i])/(G->x[i+1]-G->x[i]) - (G->f[i]-G->f[i-1])/(G->x[i]-G->x[i-1]);
    G->fp[i]=(6.0*G->fp[i]/(G->x[i+1]-G->x[i-1])-sig*G->fp[i-1])/p;
  }

  if(ypn > 1e30)
    qn = un = 0.;
  else {
    qn=0.5;
    un=(3.0/(G->x[n-1]-G->x[n-2]))*(ypn-(G->f[n-1]-G->f[n-2])/(G->x[n-1]-G->x[n-2]));
  }
  G->fpp[n-1]=(un-qn*G->fp[n-2])/(qn*G->fpp[n-2]+1.);
  for(k=n-2; k>=0; k--) G->fpp[k]=G->fpp[k]*G->fpp[k+1]+G->fp[k];

  // u = (DOUBLE*) Calloc("u", G->size, sizeof(DOUBLE));
  // use lnf array as temporary
  /*n = G->size;

  if(yp1>1e30) // zero second derivative
    G->fpp[1] = G->lnf[1]=0.;
  else { // first derivative equal to yp1
    G->fpp[1] = -0.5;
    G->lnf[1] = (3./(G->x[2]-G->x[1]))*((G->f[2]-G->f[1])/(G->x[2]-G->x[1])-yp1);
  }

  for(i=2; i<=G->size-1; i++) {
    sig = (G->x[i]-G->x[i-1])/(G->x[i+1]-G->x[i-1]);
    p = sig*G->fpp[i-1]+2.;
    G->fpp[i] = (sig-1.)/p;
    G->lnf[i] = (G->f[i+1]-G->f[i])/(G->x[i+1]-G->x[i]) - (G->f[i]-G->f[i-1])/(G->x[i]-G->x[i-1]);
    G->lnf[i] = (6.*G->lnf[i]/(G->x[i+1]-G->x[i-1])-sig*G->lnf[i-1])/p;
  }

  if(ypn>1e30) // zero second derivative
    qn = un = 0.;
  else { // first derivative equal to ypn
    qn = 0.5;
    un = (3./(G->x[n]-G->x[n-1]))*(ypn-(G->f[n]-G->f[n-1])/(G->x[n]-G->x[n-1]));
  }

  G->fpp[n] = (un-qn*G->lnf[n-1])/(qn*G->fpp[n-1]+1.);

  // second derivative
  for(k=n-1;k>=1;k--) G->fpp[k] = G->fpp[k]*G->fpp[k+1] + G->lnf[k];*/
}

/************************************ interpolate spline  *******************************/
void InterpolateSpline(struct Grid *G, DOUBLE r, DOUBLE *y, DOUBLE *yp, DOUBLE *ypp) {
  DOUBLE a,b;
  int i;

  /*int klo,khi,k; // uncomment for non uniform grid
  DOUBLE h;
  
  klo=0;
  khi=G->size-1;
  while(khi-klo > 1) {
    //k=(khi+klo) >> 1;
    k = (int)(0.5*((DOUBLE)(khi+klo)));
    if(G->x[k]>r)
      khi=k;
    else 
      klo=k;
  }
  h=G->x[khi]-G->x[klo];
  i = klo;
  if(h == 0.) Warning("Bad input to InterpolateSpline");*/

  // uniform grid
  if(r<=G->min) r = G->min; // adjust lower bound
  //i = (int) ((r-G->min)*G->I_step + 0.5);
  //i = (int) ((r-G->min)*G->I_step);
  i = (int) ((r-G->min)*G->I_step+SPLINE_EPSILON); // shift by a small amount for the check at points G->x[i]

//#ifdef SECURE
  if(i<G->size-1 && (r+SPLINE_EPSILON<G->x[i] || r-SPLINE_EPSILON>G->x[i+1])) 
    Warning("Interp. spline check failed i=%i (%" LE " <%" LE " <%" LE " )\n", i, G->x[i], r, G->x[i+1]);
//#endif
  if(i >= G->size-1) i = G->size-2; // adjust upper bound

  a = (G->x[i+1]-r)*G->I_step;
  b = (r-G->x[i])*G->I_step;

  // (3.3.3)
  *y   = a*G->f[i] + b*G->f[i+1] + ((a*a*a-a)*G->fpp[i] + (b*b*b-b)*G->fpp[i+1])*(G->step*G->step)/6.;
  *yp  = (G->f[i+1]-G->f[i])*G->I_step + ((1.-3.*a*a)*G->fpp[i]+(3.*b*b-1.)*G->fpp[i+1])*G->step/6.;
  *ypp = a*G->fpp[i] + b*G->fpp[i+1];
}

/************************ Interpolate U ************************************/
// U = ln(f)
DOUBLE InterpolateSplineU(struct Grid *G, DOUBLE r) {
  DOUBLE a,b,f;
  int i;

  if(r<=G->min) r = G->min; // adjust lower bound
  i = (int) ((r-G->min)*G->I_step+SPLINE_EPSILON); // shift by a small amount for the check at points G->x[i]
#ifdef SECURE
  if(i<G->size-1 && (r+SPLINE_EPSILON<G->x[i] || r-SPLINE_EPSILON>G->x[i+1])) Warning("  Interpolate spline U check failed, i=%i (%" LE " < %" LE "  <%" LE " )\n", i, G->x[i], r, G->x[i+1]);
#endif
  //if(i >= G->size-1) i = G->size-2; // adjust upper bound
  if(i >= G->size-1) 
#ifdef INTERPOLATE_LOG // u(r)
    //return G->f[G->size-1];
    return 0.;
#else
    //return Log(G->f[G->size-1]);
    return 0.;
#endif

  a = (G->x[i+1]-r)*G->I_step;
  b = (r-G->x[i])*G->I_step;

  f = a*G->f[i] + b*G->f[i+1] + ((a*a*a-a)*G->fpp[i] + (b*b*b-b)*G->fpp[i+1])*(G->step*G->step)/6.;

#ifdef INTERPOLATE_LOG // u(r)
  return f;
#else // f(r)
#ifdef SECURE
  if(f<0) Warning("Interpolate spline, ln() of negative argument\n");
#endif
  if(f<0) return -1e8;

  return Log(f);
#endif
}

/************************ Interpolate F ************************************/
// same as U, but allows negative values (i.e. interpolation of Vint(r))
DOUBLE InterpolateSplineF(struct Grid *G, DOUBLE r) {
  DOUBLE a,b,f;
  int i;

  if(r<=G->min) r = G->min; // adjust lower bound
  i = (int) ((r-G->min)*G->I_step+SPLINE_EPSILON); // shift by a small amount for the check at points G->x[i]
  //if(i >= G->size-1) i = G->size-2; // adjust upper bound
  if(i >= G->size-1) return 0.;

  a = (G->x[i+1]-r)*G->I_step;
  b = (r-G->x[i])*G->I_step;

  f = a*G->f[i] + b*G->f[i+1] + ((a*a*a-a)*G->fpp[i] + (b*b*b-b)*G->fpp[i+1])*(G->step*G->step)/6.;

  return f;
}

/************************ InterpolateSplineFp ******************************/
// fp = f'/f
DOUBLE InterpolateSplineFp(struct Grid *G, DOUBLE r) {
  DOUBLE a,b;
  int i;
#ifndef INTERPOLATE_LOG
  DOUBLE fp, f;
#endif

  if(r<=G->min) r = G->min; // adjust lower bound
  i = (int) ((r-G->min)*G->I_step+SPLINE_EPSILON); // shift by a small amount for the check at points G->x[i]
#ifdef SECURE
  if(i<G->size-1 && (r+SPLINE_EPSILON<G->x[i] || r-SPLINE_EPSILON>G->x[i+1])) Warning("  Interpolate spline Fp check failed, i=%i (%" LE " < %" LE "  <%" LE " )\n", i, G->x[i], r, G->x[i+1]);
#endif
  //if(i >= G->size-1) i = G->size-2; // adjust upper bound
  if(i >= G->size-1) return 0.;

  a = (G->x[i+1]-r)*G->I_step;
  b = (r-G->x[i])*G->I_step;

#ifdef INTERPOLATE_LOG // f'/f = u'
  return (G->f[i+1]-G->f[i])*G->I_step + ((1.-3.*a*a)*G->fpp[i]+(3.*b*b-1.)*G->fpp[i+1])*G->step/6.;
#else // f'/f
  f = a*G->f[i] + b*G->f[i+1] + ((a*a*a-a)*G->fpp[i] + (b*b*b-b)*G->fpp[i+1])*(G->step*G->step)/6.;
  fp  = (G->f[i+1]-G->f[i])*G->I_step + ((1.-3.*a*a)*G->fpp[i]+(3.*b*b-1.)*G->fpp[i+1])*G->step/6.;
#ifdef SECURE
  if(f<0) Warning("Interpolate spline Fp of negative argument\n");
#endif
  if(f<0) return 0;

  return fp/f;
#endif
}

/************************ InterpolateSpline E*******************************/
// Eloc = -(f" +2/r f')/f+(f'/f)^2
DOUBLE InterpolateSplineE(struct Grid *G, DOUBLE r) {
  DOUBLE a,b;
  int i;
#ifdef INTERPOLATE_LOG
  DOUBLE up,upp;
#else
  DOUBLE f,fp,fpp;
#endif

  if(r == 0) return 0.;

  if(r<=G->min) r = G->min; // adjust lower bound
  i = (int) ((r-G->min)*G->I_step+SPLINE_EPSILON); // shift by a small amount for the check at points G->x[i]
#ifdef SECURE
  if(i<G->size-1 && (r+SPLINE_EPSILON<G->x[i] || r-SPLINE_EPSILON>G->x[i+1])) Warning("  Interpolate spline E check failed, i=%i (%" LE " < %" LE "  <%" LE " )\n", i, G->x[i], r, G->x[i+1]);
#endif
  //if(i >= G->size-1) i = G->size-2; // adjust upper bound
  if(i >= G->size-1) return 0.;

  a = (G->x[i+1]-r)*G->I_step;
  b = (r-G->x[i])*G->I_step;

#ifdef INTERPOLATE_LOG // (-f" + 2/r f')/f + (f'/f)^2 = - u" - u' 2/r
  up  = (G->f[i+1]-G->f[i])*G->I_step + ((1.-3.*a*a)*G->fpp[i]+(3.*b*b-1.)*G->fpp[i+1])*G->step/6.;
  upp = a*G->fpp[i] + b*G->fpp[i+1];

#ifdef TRIAL_1D
  return - upp;
#endif
#ifdef TRIAL_2D
  return - upp - up/r;
#endif
#ifdef TRIAL_3D
  return - upp - 2.*up/r;
#endif
#else // (-f" + 2/r f')/f + (f'/f)^2
  f = a*G->f[i] + b*G->f[i+1] + ((a*a*a-a)*G->fpp[i] + (b*b*b-b)*G->fpp[i+1])*(G->step*G->step)/6.;
  fp  = (G->f[i+1]-G->f[i])*G->I_step + ((1.-3.*a*a)*G->fpp[i]+(3.*b*b-1.)*G->fpp[i+1])*G->step/6.;
  fpp = a*G->fpp[i] + b*G->fpp[i+1];

  fp /= f;
  fpp  /= f;

  if(r==0.) return 0.;
#ifdef SECURE
  if(f<0) Warning("Interpolate spline E of negative argument\n");
#endif
  if(f<0) return 0;

#ifdef TRIAL_1D
  return -fpp + fp*fp;
#endif
#ifdef TRIAL_2D
  return -fpp - fp/r + fp*fp;
#endif
#ifdef TRIAL_3D
  return -fpp - 2.*fp/r + fp*fp;
#endif

#endif
}

/************************** Linear Interpolation Construct ******************************/
// initialize Fp and E
void LinearInterpolationConstruct(struct Grid *G) {
  int i, k;
  DOUBLE p, qn, sig, un;
  DOUBLE Fp, Fpp;
  DOUBLE dr,dr2;
  int n;

  n = G->size;

  /*if(yp1>1e30) // zero second derivative
    G->fpp[0] = G->fp[0]=0.;
  else { // first derivative equal to yp1
    G->fpp[0] = -0.5;
    G->fp[0] = (3./(G->x[1]-G->x[0]))*((G->f[1]-G->f[0])/(G->x[1]-G->x[0])-yp1);
  }*/

  /*Message("  damping two-body solution\n");
  Message("x f(x) Escat\n");
  dr = G->step;
  dr2 = dr*dr;

  for(i=1; i<n-1; i++) {
    Fp = (G->f[i+1]-G->f[i-1])*0.5/dr;
    Fpp = (G->f[i+1]-2.*G->f[i]+G->f[i-1])/dr2;

    Fp /= G->f[i];
    Fpp /= G->f[i];

    //Message("%lf %e %e\n", G->x[i], G->f[i], -(Fpp +(DIMENSION-1.)*Fp/G->x[i]) + InteractionEnergy(G->x[i]));
  }*/

  for(i=1; i<n-1; i++) {
    Fp = (G->f[i+1]-G->f[i-1])/(G->x[i+1]-G->x[i-1]);
    Fpp = (G->f[i+1]-2.*G->f[i]+G->f[i-1])/(G->x[i+1]-G->x[i])/(G->x[i]-G->x[i-1]);

    Fp /= G->f[i];
    Fpp /= G->f[i];

    G->lnf[i] = log(G->f[i]);
    G->fp[i] = Fp;
//3D Eloc = [-(f"+2f'/r)/f] + (f'/f)^2
//2D Eloc = [-(f" + f'/r)/f] + (f'/f)^2
//1D Eloc = [-f"/f] + (f'/f)^2
    G->E[i] = -(Fpp +(DIMENSION-1.)*Fp/G->x[i]) + Fp*Fp;
  }

  G->lnf[0] = log(G->f[0]);
  G->fp[0] = G->fp[1];
  G->E[0] = G->E[1];

  G->lnf[n-1] = log(G->f[n-1]);
  G->fp[n-1] = G->fp[n-2];
  G->E[n-1] = G->E[n-2];

  //Warning("  Pair interaction potential is added in the Jastrow spline\n");
  //for(i=0; i<n; i++) {
  //  G->E[i] += InteractionEnergy(G->x[i]);
  //}
  //Warning("  Setting: gaussian_alpha = gaussian_beta = 0\n");
  //gaussian_alpha = gaussian_beta = 0;
}
